var TableModel = Backbone.Model.extend({
	defaults: {
		name: 'name',
		summ: 0,
		count:0
	},
	initialize: function (){},
	validate : function (attrs) {
		if (!(attrs.summ) < 0 ) {
			console.log("Incorrect summ");
			//return "Incorrect summ"
		}else if (!(attrs.count) < 0 ) {
			console.log("Incorrect count");
			//return "Incorrect count"
		}
	}
});

var TablesCollection = Backbone.Collection.extend({
		model: TableModel,
		sortParam: "summ",
		sortMode: 1,
		comparator: function (a,b) {
		if (a.get(this.sortParam) > b.get(this.sortParam)) return -1*this.sortMode;
        if (a.get(this.sortParam) < b.get(this.sortParam)) return this.sortMode;
        return 0;
		}
});
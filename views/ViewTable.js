var TableView = Backbone.View.extend({

  tagName: "tr",

  events: {
    'click .changeSumm':  'changeSumm',
    'click .changeCount': 'changeCount',
    'click .deleteRow':   'deleteRow',
    'blur .name, .summ, .count': 'editValue'
  },

  initialize: function() {
    this.template = _.template($('#ViewTable').html());
    this.listenTo(this.model,'change',this.render);
    this.listenTo(this.model,'destroy',this.remove)
  },

  render: function() {
    var view = this.template(this.model.toJSON());
    this.$el.html(view);
    return this.$el;
  },
  deleteRow: function () {
    this.model.destroy();
  },
  editValue: function() {
    var res = this.model.set({
      name: this.$(".name").text(),
      summ: parseInt(this.$("input.summ").attr('value')),
      count: parseInt(this.$("input.count").attr('value'))
    },{validate: true});
    if (!res) this.render();
  },
  changeSumm: function(ev) {
   var diff = parseInt($(ev.target).attr('data-rel'));
   var summ = this.model.get('summ');
   var res = this.model.set({
      summ: summ+diff
   });
   if (!res) this.render();
  },
  changeCount: function(event){
    var cnt = parseInt($(event.target).attr('data-cnt'));
    var count = this.model.get('count');
    var res = this.model.set({
      count: count+cnt
  });
    if (!res) this.render();
  }
});
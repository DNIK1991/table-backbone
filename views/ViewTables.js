var TablesView = Backbone.View.extend({

  events: {
    'click .addObject': 'addObject',
   	'click .toJSON': 'toJSON',
   	'click [data-sort]': 'renderList'
  },

  initialize: function() {
    this.template = _.template($('#ViewTables').html());
    this.$el.html(this.template());
    this.coll = new TablesCollection();
    this.listenTo(this.coll, 'all', this.render);
    this.listenTo(this.coll, 'add', this.addOne);
  },

  render: function() {
  	var count = 0;
  	var summ = 0;
  	var result = 0;
  	this.coll.each(function(obj,index){
  		count += obj.get('summ');
  		summ += obj.get('count');
        result += summ * count;
  	});
  	this.$('.tables-count').text(this.coll.length);
  	this.$('.tables-summ').text(result);
  },

  addObject: function() {
  	this.coll.add({});
  },
  addOne: function(model){
  	var view = new TableView({model: model});
  	this.$('.tablesList').append(view.render());
  },
  renderList: function(e){
  	this.$('tablesList').html('');
  	this.coll.sortParam = $(e.target).attr('data-sort');
  	this.coll.sortMode = this.coll.sortMode*(-1);
  	this.coll.sort();
  },
  toJSON: function() {
  	var json = this.coll.toJSON();
  	this.$('.json-out').html(JSON.stringify(json));
  }
});